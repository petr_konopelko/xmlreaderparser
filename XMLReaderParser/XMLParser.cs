﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XMLReaderParser
{
    public class XMLParser
    {
        public string GetXmlString(XmlReader reader)
        {
            StringBuilder builder = new StringBuilder();
            while (reader.Read())
            {
                string element = GetCurrentElement(reader);
                builder.AppendLine(element);
            }
            return builder.ToString();
        }

        private string GetCurrentElement(XmlReader reader)
        {
            String element = String.Empty;
            switch (reader.NodeType)
            {
                case XmlNodeType.Element:
                    List<String> attributes = new List<String>();
                    attributes = GetAttributes(reader);
                    reader.MoveToElement();
                    element = String.Format("<{0}{1}>", reader.Name, attributes.Any() ? string.Join(" ", attributes) : String.Empty);
                    break;
                case XmlNodeType.EndElement:
                    element = $"</{reader.Name}>";
                    break;

            }

            return element;
        }

        private List<String> GetAttributes(XmlReader reader)
        {
            List<String> attributes = new List<String>();
            if (reader.HasAttributes)
            {
                while (reader.MoveToNextAttribute())
                {
                    attributes.Add($"{reader.Name} = \"{reader.Value}\"");
                }
            }
            return attributes;
        }
    }
}
